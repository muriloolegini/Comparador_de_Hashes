import hashlib

arquivo1 = r"C:\Users\Downloads\ComparadorHashes\src\a.txt"
arquivo2 = r"C:\Users\Downloads\ComparadorHashes\src\b.txt"

hash1 = hashlib.new('ripemd160')
hash1.update(open(arquivo1, 'rb').read())

hash2 = hashlib.new('ripemd160')
hash2.update(open(arquivo2, 'rb').read())

if hash1.digest() != hash2.digest():
    print(f'O arquivo {"1"} é diferente do arquivo {"2"}')
    print('O hash do arquivo a.txt é: ', hash1.hexdigest())
    print('O hash do arquivo b.txt é: ', hash2.hexdigest())
else:
    print(f'O arquivo {"1"} é igual ao arquivo {"2"}')
    print('O hash do arquivo a.txt é: ', hash1.hexdigest())
    print('O hash do arquivo b.txt é: ', hash2.hexdigest())